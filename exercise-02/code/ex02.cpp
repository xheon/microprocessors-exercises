#include <iostream>
#include <chrono>
#include <fstream>

using namespace std::chrono;

struct memblock
{
    char data[64-sizeof(size_t)];
    size_t next;
};


double loop(int N)
{    

    // Init
    int stride = 65;
    memblock blocks[N];
    for(int i = 0; i < N; ++i)
    {
        blocks[i].next = (i+stride) % N;
    }

    int counter = 0;
    int next = 0;
    for(int i = 0; i < N; ++i)
    {
        next = blocks[next].next;

        if(next == 0) counter++;
    }

    if(counter > 1)
    {
        std::cout << "Counter > 1: " << counter << std::endl;
    }

    const int accesses = 1000000;
    time_point<system_clock> start, end;
    start = system_clock::now();

    next = 0;
    for(int i = 0; i < accesses; ++i)
    {
        next = blocks[next].next;
    }

    end = system_clock::now();
    double time = std::chrono::duration_cast<nanoseconds>(end - start).count();
    time /= accesses;

    std::cout << "Time/Access for " << N << ": " << time << std::endl;

    return time;
}

int main()
{
    std::ofstream file;
    file.open("results.csv");

    std::ofstream xValues;
    xValues.open("xvals.csv");

    int runs = 2000;
    int N = 0;
    for(int run = 0; run < runs; ++run)
    {
        N += 64;
        if (N % 5 == 0 || N % 13 == 0 || N % 65 == 0) continue;

        double time = loop(N);
        file << time << ";";
        xValues << ((N*64)/1024.0f) << ";";
    }

    file.close();
    xValues.close();

    return 0;
}

