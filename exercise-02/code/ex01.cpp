#include <iostream>
#include <chrono>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std::chrono;

const int N = 1024 * 4096;

int main()
{
    char array[N];

    // Init array with random numbers
    srand(time(NULL));

    for(int i = 0; i < N; i++)
    {
        array[i] = rand() % 128;
    }

    std::ofstream file;
    file.open("results.csv");
    time_point<system_clock> start, end;
    int maxStride = 512;
    for(int stride = 1; stride < maxStride; stride++)
    {
        int runs = 100;

        std::vector<double> times;
        times.reserve(runs);

        int result;
        for(int run = 0; run < runs; run++)
        {
            start = system_clock::now();
            result = 0;
            for(int i = 0; i < N; i += stride)
            {
                result += array[i];
            }
            end = system_clock::now();
            double time = std::chrono::duration_cast<nanoseconds>(end - start).count();

            time /= (N/stride);
            times.push_back(time);
        }

        // calculate mean value
        std::sort(times.begin(), times.end());

        double meanValue = times[runs/2];


        std::cout << "Stride: " << stride << ", \t" << meanValue << ", \t" << result << std::endl;
        file << meanValue << ";";
    }

    file.close();

    return 0;
}

